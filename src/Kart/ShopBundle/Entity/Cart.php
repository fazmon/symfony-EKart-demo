<?php

namespace Kart\ShopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


class Cart
{

    /**
     * @var integer
     */
    private $userId;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Kart\ShopBundle\Entity\Product
     */
    private $product;


    /**
     * Set userId
     *
     * @param integer $userId
     * @return Cart
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer 
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set product
     *
     * @param \Kart\ShopBundle\Entity\Product $product
     * @return Cart
     */
    public function setProduct(\Kart\ShopBundle\Entity\Product $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \Kart\ShopBundle\Entity\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }
}
