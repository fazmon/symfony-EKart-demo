<?php
/**
 * Created by PhpStorm.
 * User: fasalu
 * Date: 26/1/18
 * Time: 10:41 PM
 */

namespace Kart\ShopBundle\Controller;


use Kart\ShopBundle\Entity\Cart;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session;

class CartController extends Controller
{

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser()->getId();

        $cartItems = $em->getRepository('KartShopBundle:Cart')->findCartItemByUser($user);

        $totalCartValue = 0;
        foreach($cartItems as $cartItem)
        {
            $totalCartValue = $totalCartValue+$cartItem->getProduct()->getPrice();
        }

        return $this->render('KartShopBundle:Cart:cart.html.twig',
            array('cartItems' => $cartItems, 'totalValue' => $totalCartValue));
    }

    public function addItemAction($itemId = null)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $product = $this->getDoctrine()->getRepository('KartShopBundle:Product')->find($itemId);
        $cart = new Cart();

        $cart->setUserId($user->getId());
        $cart->setProduct($product);

        $em->persist($cart);
        $em->flush($cart);

        $this->get('session')->getFlashBag()
            ->add('success', 'Your item has been added to cart successfully!');
        return $this->redirectToRoute('kart_shop_products_listing');

    }

    public function checkoutAction()
    {


        return $this->render('KartShopBundle:Cart:checkout.html.twig');


    }


}