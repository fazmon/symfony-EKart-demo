<?php

namespace Kart\ShopBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Kart\ShopBundle\Entity\Product;

/**
 * Product controller.
 *
 */
class ProductController extends Controller
{

    /**
     * Lists all Products.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();

        $products = $em->getRepository('KartShopBundle:Product')->findAll();

        return $this->render('KartShopBundle:Product:product_listing.html.twig', array(
            'products' => $products,
            'user' => $user
        ));
    }

}
