<?php

namespace Kart\ShopBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Kart\UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('KartShopBundle:Default:index.html.twig');
    }
//public function userHomeAction()
//    {
//        return $this->render('KartShopBundle:Default:userHome.html.twig');
//
//    }

//    public function userHomeAction()
//    {
//        return $this->render('KartShopBundle:Default:userHome.html.twig');
//
//    }


    public function registerAction(Request $request)
    {

        $username = $request->get('username');
        $email = $request->get('email');
        $password = $request->get('password');

        $em = $this->getDoctrine()->getManager();
        $user = new User();

        $user->setUsername($username);
        $user->setEmail($email);
        $user->setPassword($password);

        $em->persist($user);
        $em->flush($user);

        return $this->render('KartShopBundle:Default:index.html.twig');

    }



}
